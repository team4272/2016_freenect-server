/* wg.h - Thread management tools modeled on
 * https://golang.org/pkg/sync/#WaitGroup
 *
 * Copyright (C) 2016  Luke Shumaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <pthread.h>

/* When you call wg_wait, the waitgroup is destroyed.  You must
 * re-wg_init it if you want to reuse it.
 *
 * Call wg_add() right before you call pthread_create, and call
 * wg_sub() right before your thread exits.  In your master process,
 * call wg_wait() to wait for all threads to finish.
 */

struct wg {
	int count;
	pthread_mutex_t lock;
	int fd_threads[2];
	pthread_t gc;
};

void wg_init(struct wg *);
void wg_add(struct wg *);
void wg_sub(struct wg *);
void wg_wait(struct wg*);
