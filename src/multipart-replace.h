/* multipart-replace.h - handle mixed/x-multipart-replace streams
 *
 * Copyright (C) 2016  Luke Shumaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <pthread.h>
#include <stdbool.h>

struct frame {
	ssize_t len;
	size_t cap;
	char *buf;
};

struct multipart_replace_stream {
	struct frame a, b;
	struct frame *front, *back;
	pthread_rwlock_t frontlock;
	pthread_cond_t newframe;
	pthread_mutex_t newframe_lock;
};

int multipart_replace_reader(struct multipart_replace_stream *s, int fd, const char *boundary);
int multipart_replace_writer(struct multipart_replace_stream *s, int fd, const char *boundary);
void init_multipart_replace_stream(struct multipart_replace_stream *s);
void destroy_multipart_replace_stream(struct multipart_replace_stream *s);

extern bool running;
