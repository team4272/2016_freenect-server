/* util.h - miscellaneous utility functions
 *
 * Copyright (C) 2016  Luke Shumaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <signal.h> /* for sig_atomic_t */
#include <stdbool.h>
#include <stdio.h> /* for FILE* */
#include <string.h> /* for memset(3) */

#define UNUSED __attribute__((__unused__))
#define ZERO(x) memset(&(x), 0, sizeof(x))

#ifndef _
#define _(str) str
#endif

#define log(...) error(0, 0, __VA_ARGS__)
#define debug(...) error(0, 0, __VA_ARGS__)

void *xrealloc(void *ptr, size_t size);
bool is_numeric(const char *str);
int get_fd(const char *addr);

FILE *xfopen(const char *path, const char *mode);
FILE *xfdopen(const char *path, const char *mode);


int sockstream_listen(const char *type, const char *addr);
