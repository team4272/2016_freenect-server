# Copyright (C) 2016  Luke Shumaker
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The source is C99 written for GNU libc
CFLAGS   += -std=c99
CPPFLAGS += -std=c99
CPPFLAGS += -D_GNU_SOURCE

# Be hyper-strict
CFLAGS   += -Wall -Werror -Wextra -pedantic
CPPFLAGS += -Wall -Werror -Wextra -pedantic

# Debugging
CFLAGS   += -g -fvar-tracking-assignments
CPPFLAGS += -g -fvar-tracking-assignments

# Enable the stack protector
CFLAGS += -fstack-protector

# Enable FORTIFY_SOURCE
CFLAGS   += -O2
CPPFLAGS += -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=2

# The main Makefile segment
all: build
.PHONY: all

include config.mk

progs = freenect-server multipart-replace-http-server
units = freenect-server-http.service freenect-server-http.socket freenect-server.service

build: $(addprefix src/,$(progs)) $(addprefix systemd/,$(units))
.PHONY: build

install: $(addprefix $(DESTDIR)$(bindir)/,$(progs)) $(addprefix $(DESTDIR)$(systemddir)/,$(units))
.PHONY: install

# Automatically figure out C header dependencies
CPPFLAGS += -MD -MF $(@D)/$(@F:%.o=.%.mk) -MP
-include src/.*.mk

# Dependencies that don't get figured out automatically
src/freenect-server: src/util.o -lsystemd -lfreenect -lusb-1.0 -ljpeg
src/multipart-replace-http-server: src/util.o src/wg.o src/multipart-replace.o -lsystemd -lpthread
systemd/freenect-server-http.service: .var.user .var.group .var.bindir
systemd/freenect-server-http.socket: .var.httpstream
systemd/freenect-server.service: .var.user .var.group .var.bindir

# Rules to make things
COPYING.GPL3:
	wget http://www.gnu.org/licenses/gpl-3.0.txt -O $@

systemd/%: systemd/%.in
	sed $(foreach v,$(patsubst .var.%,%,$(filter .var.%,$^)), -e 's|@$v@|$($v)|' ) < $< > $@
$(DESTDIR)$(bindir)/%: src/%
	install -Dm755 $< $@
$(DESTDIR)$(systemddir)/%: systemd/%
	install -Dm644 $< $@
.var.%: FORCE
	@printf '%s' '$($*)' | ./write-ifchanged $@

# Undo
clean:
	rm -f -- src/*.o src/.*.mk
	rm -f -- $(addprefix src/,$(progs)) $(addprefix systemd/,$(units))
.PHONY: clean

uninstall:
	rm -f -- $(addprefix $(DESTDIR)$(bindir)/,$(progs)) $(addprefix $(DESTDIR)$(systemddir)/,$(units))
	rmdir -p -- $(dir $(addprefix $(DESTDIR)$(bindir)/,$(progs)) $(addprefix $(DESTDIR)$(systemddir)/,$(units))) 2>/dev/null || true
.PHONY: uninstall

# Noise that I put in every Makefile
.PHONY: FORCE
.DELETE_ON_ERROR:
.SECONDARY:
